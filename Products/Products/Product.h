#pragma once
#include <string>
#include <cstdint>
#include "IPriceable.h"

class Product :public IPriceable
{
public:
	Product(int32_t id, const std::string& name, float rawPrice); //in uint_16 id

	uint16_t getID() const; //folosim const pentru a nu modifica this
	const std::string& getName() const;
	float getRawPrice() const;

protected:
	int32_t m_id;
	std::string m_name;
	float m_rawPrice;
};